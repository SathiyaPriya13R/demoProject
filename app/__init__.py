from flask import Flask
from flask_cors import CORS
from app.api.schedule import schedule_bp
from app.api.upload import upload_bp
from app.api.email import email_bp
from app.api.user import user_bp
from app.api.candidate import candidate_bp
from app.api.basic_info import basicInfo_bp
from app.api.contact import contact_bp
from app.api.certificate import certificate_bp
from app.api.additional_documents import additional_bp
from app.api.marketing import marketing_bp
from app.config import Config
import os  # Import the os module

# Inside your create_app function in __init__.py

def create_app():
    app = Flask(__name__)
    CORS(app, resources={r"/api/*": {"origins": "http://localhost:3000"}})
    app.config.from_object(Config)

    app.register_blueprint(schedule_bp, url_prefix='/api/schedule')
    app.register_blueprint(upload_bp, url_prefix='/api')
    app.register_blueprint(email_bp, url_prefix='/api/email')
    app.register_blueprint(user_bp, url_prefix='/api/user')  # Use register_blueprint here
    app.register_blueprint(candidate_bp, url_prefix='/api/candidate')    
    app.register_blueprint(basicInfo_bp, url_prefix='/api/basic_info') 
    app.register_blueprint(contact_bp, url_prefix='/api/contact') 
    app.register_blueprint(certificate_bp, url_prefix='/api/certificate') 
    app.register_blueprint(additional_bp, url_prefix='/api/additional_documents') 
    app.register_blueprint(marketing_bp, url_prefix='/api/marketing') 
    
    
    if not os.path.exists(Config.UPLOAD_FOLDER):
        os.makedirs(Config.UPLOAD_FOLDER)

    return app

