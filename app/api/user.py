# from flask import Flask, Blueprint, request, jsonify
# from werkzeug.security import generate_password_hash, check_password_hash
# from flask_cors import CORS, cross_origin
# from app.db.mongo_client import users_collection
 
# user_bp = Blueprint('user', __name__, url_prefix='/api/user')
 
# @user_bp.after_request
# def add_cors_headers(response):
#     response.headers['Access-Control-Allow-Origin'] = '*'
#     response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
#     response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
#     return response
 
# @user_bp.route('/register', methods=['POST'])
# @cross_origin(origins='http://localhost:3000')
# def register():
#     data = request.json
#     name = data.get('name')
#     email = data.get('email')
#     password = data.get('password')
#     phoneNumber = data.get('phoneNumber')
 
#     if not name or not email or not password or not phoneNumber:
#         return jsonify({'error': 'Please provide name, email, password, and phone number'}), 400
 
#     # Check if the user already exists
#     if users_collection.find_one({'email': email}):
#         return jsonify({'error': 'Email address already registered'}), 400
 
#     # Hash the password before storing it
#     hashed_password = generate_password_hash(password)
 
#     # Create the user document
#     user = {
#         'name': name,
#         'email': email,
#         'password': hashed_password,
#         'phoneNumber': phoneNumber
#     }
 
#     # Insert the user document into the collection
#     users_collection.insert_one(user)
 
#     return jsonify({'message': 'User registered successfully'}), 201
 
# @user_bp.route('/login', methods=['GET'])
# @cross_origin(origins='http://localhost:3000')
# def login():
#     email = request.args.get('email')
#     password = request.args.get('password')
 
#     if not email or not password:
#         return jsonify({'error': 'Please provide email and password'}), 400
 
#     user = users_collection.find_one({'email': email})
 
#     if user and check_password_hash(user['password'], password):
#         return jsonify({'message': 'Login successful', 'name': user['name'], 'email': user['email']}), 200
#     else:
#         return jsonify({'error': 'Invalid email or password'}), 400
    


from flask import Flask, Blueprint, request, jsonify
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import CORS, cross_origin
from pymongo import MongoClient, errors
from urllib.parse import quote_plus
import random
import string
import sendgrid
from sendgrid.helpers.mail import Mail
import logging
from app.db.mongo_client import users_collection




user_bp = Blueprint('user', __name__, url_prefix='/api/user')
 
@user_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response

 
# SendGrid API key (replace with your own)
SENDGRID_API_KEY = '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'
 
# Configure logging
logging.basicConfig(level=logging.DEBUG)
 
def send_verification_email(email, otp):
    try:
        sg = sendgrid.SendGridAPIClient(api_key=SENDGRID_API_KEY)
        from_email = 'guberanrealme@gmail.com'  # Replace with your verified sender email
        subject = 'Email Verification'
        content = f'Your verification code is: {otp}'
        mail = Mail(
            from_email=from_email,
            to_emails=email,
            subject=subject,
            plain_text_content=content,
            html_content=f'<p>Your verification code is: <strong>{otp}</strong></p>'
        )
        response = sg.client.mail.send.post(request_body=mail.get())
        if response.status_code == 202:
            logging.info(f"Email sent successfully to {email}")
        else:
            logging.error(f"Error sending email: {response.body}")
            raise Exception("Failed to send verification email")
    except Exception as e:
        logging.error(f"Error sending verification email: {e}")
        raise e
 
@user_bp.route('/register', methods=['POST'])
@cross_origin(origins='http://localhost:3000')
def register():
    try:
        data = request.json
        logging.debug(f"Received registration data: {data}")
        name = data.get('name')
        email = data.get('email')
        password = data.get('password')
        phoneNumber = data.get('phoneNumber')
 
        if not name or not email or not password or not phoneNumber:
            return jsonify({'error': 'Please provide name, email, password, and phone number'}), 400
 
        if not validate_email(email):
            return jsonify({'error': 'Invalid email format'}), 400
 
        if not validate_phone_number(phoneNumber):
            return jsonify({'error': 'Invalid phone number'}), 400
 
        if not validate_password(password):
            return jsonify({'error': 'Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one number, and one special character'}), 400
 
        if users_collection.find_one({'email': email}):
            return jsonify({'error': 'Email address already registered'}), 400
 
        hashed_password = generate_password_hash(password)
        otp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
 
        user = {
            'name': name,
            'email': email,
            'password': hashed_password,
            'phoneNumber': phoneNumber,
            'otp': otp,
            'verified': False
        }
 
        users_collection.insert_one(user)
        send_verification_email(email, otp)
 
        return jsonify({'message': 'User registered successfully. Verification email sent.'}), 201
 
    except errors.PyMongoError as e:
        logging.error(f"MongoDB error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
    except Exception as e:
        logging.error(f"Error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
 
@user_bp.route('/validate', methods=['POST'])
@cross_origin(origins='http://localhost:3000')
def validate():
    try:
        data = request.json
        logging.debug(f"Received OTP validation data: {data}")
        email = data.get('email')
        otp = data.get('otp')
 
        user = users_collection.find_one({'email': email})
 
        if user and user['otp'] == otp:
            users_collection.update_one({'email': email}, {'$set': {'verified': True}})
            return jsonify({'success': True, 'message': 'Email verified successfully'}), 200
        else:
            return jsonify({'success': False, 'error': 'Invalid OTP'}), 400
 
    except errors.PyMongoError as e:
        logging.error(f"MongoDB error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
    except Exception as e:
        logging.error(f"Error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
 
@user_bp.route('/login', methods=['GET'])
@cross_origin(origins='http://localhost:3000')
def login():
    email = request.args.get('email')
    password = request.args.get('password')
 
    if not email or not password:
        return jsonify({'error': 'Please provide email and password'}), 400
 
    user = users_collection.find_one({'email': email})
 
    if user and check_password_hash(user['password'], password):
        if not user.get('verified', False):
            return jsonify({'error': 'Email not verified'}), 400
        return jsonify({'message': 'Login successful', 'name': user['name'], 'email': user['email']}), 200
    else:
        return jsonify({'error': 'Invalid email or password'}), 400
 
def validate_email(email):
    import re
    email_regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    return re.match(email_regex, email) is not None
 
def validate_password(password):
    import re
    password_regex = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
    return re.match(password_regex, password) is not None
 
def validate_phone_number(phone_number):
    import re
    phone_regex = r'^\d{10}$'
    return re.match(phone_regex, phone_number) is not None
 


