from flask import Flask, jsonify, request,Blueprint
from flask_cors import CORS
from pymongo import MongoClient
from app.db.mongo_client import contact_collection


contact_bp = Blueprint('contact', __name__, url_prefix='/api/contact')
 
@contact_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response



# Define your routes here
@contact_bp.route('/contact', methods=['POST'])
def save_contact():
    data = request.json
    try:
        # Insert the contact information into the MongoDB collection
        contact_collection.insert_one(data)
        return jsonify({'message': 'Contact information saved successfully'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


