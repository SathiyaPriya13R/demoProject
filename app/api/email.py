from flask import Blueprint, request, jsonify
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from pymongo import MongoClient
from app.db.mongo_client import get_email_collection
import secrets
import os
from flask_cors import cross_origin
from app.config import Config

email_bp = Blueprint('email', __name__)

SENDGRID_API_KEY = "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"
FROM_EMAIL = 'guberanrealme@gmail.com'

@email_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response

@email_bp.route('/send', methods=['POST'])
@cross_origin(origins='http://localhost:3000')  # Enable CORS for this route
def send_email():
    data = request.json
    if not data:
        return jsonify({'message': 'No data provided'}), 400

    org_email = data.get('org_email')
    name = data.get('name')
    organisation = data.get('organisation')
    phone = data.get('phone')
    message = data.get('message')
    date = data.get('date')

    if not all([org_email, name, organisation, phone, message, date]):
        return jsonify({'message': 'Missing required fields'}), 400

    meeting_link = generate_meeting_link()

    try:
        collection = get_email_collection()
        collection.insert_one({
            "org_email": org_email,
            "name": name,
            "organisation": organisation,
            "phone": phone,
            "message": message,
            "date": date,
            "meeting_link": meeting_link
        })
    except Exception as e:
        print(f"Error saving data to MongoDB: {e}")
        return jsonify({'message': 'Error saving data to MongoDB', 'error': str(e)}), 500

    html_content = generate_email_html(date, meeting_link, org_email)

    email_message = Mail(
        from_email=FROM_EMAIL,
        to_emails=org_email,
        subject=f'Updated Event Invitation\n{organisation} has made changes to ViTech Demo',
        html_content=html_content
    )

    try:
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        response = sg.send(email_message)
        if response.status_code == 202:
            return jsonify({'message': 'Email sent successfully'}), 200
        else:
            print(f"Failed to send email: {response.status_code}, {response.body}")
            return jsonify({'message': 'Failed to send email', 'status_code': response.status_code}), 500
    except Exception as e:
        print(f"Error sending email: {e}")
        return jsonify({'message': 'Error sending email', 'error': str(e)}), 500

def generate_meeting_link():
    return f"https://example.com/meeting/{secrets.token_urlsafe(10)}"

def generate_email_html(date, meeting_link, org_email):
    return f"""\
    <html>
    <body>
        <h3>Updated Event Invitation</h3>
        <p>ViTech Demo</p>
        <p>Date and Time: {date}</p>
        <p>Participants: {org_email}</p>
        <p>Join with Meet: <a href="{meeting_link}">{meeting_link}</a></p>
        <p>Thank You.</p>
    </body>
    </html>
    """
