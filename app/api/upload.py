from flask import Blueprint, request, jsonify
from werkzeug.utils import secure_filename
import os
from app.core.pdf_parser import parse_resume

upload_bp = Blueprint('upload', __name__)

UPLOAD_FOLDER = './uploads'
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

@upload_bp.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'message': 'No file part'}), 400

    file = request.files['file']
    if file.filename == '':
        return jsonify({'message': 'No selected file'}), 400

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file_path = os.path.join(UPLOAD_FOLDER, filename)
        file.save(file_path)
        parsed_data = parse_resume(file_path)
        print(parsed_data,"hello")
        return jsonify({'message': 'File successfully uploaded', 'filename': filename, 'parsed_data': parsed_data}), 200

    return jsonify({'message': 'File type not allowed'}), 400

def allowed_file(filename):
    allowed_extensions = {'pdf'}
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allowed_extensions
