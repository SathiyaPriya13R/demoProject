from flask import Flask, request, jsonify,Blueprint
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus
from app.db.mongo_client import additional_collection

additional_bp = Blueprint('additional_documents', __name__, url_prefix='/api/additional_documents')
 
@additional_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response


@additional_bp.route('/additionaldoc', methods=['POST'])
def upload_additional_documents():
    try:
        file = request.files['file']
        name = request.form['name']

        # Save the file to MongoDB GridFS
        file_id = additional_collection.insert_one({'name': name, 'file': file.read()}).inserted_id
        
        return jsonify({'message': 'Additional document saved successfully', 'file_id': str(file_id)})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


