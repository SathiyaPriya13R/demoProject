from flask import Flask, request, jsonify,Blueprint
from pymongo import MongoClient
from urllib.parse import quote_plus
from flask_cors import CORS
import uuid
from flask_cors import cross_origin
from app.db.mongo_client import candidate_collection


candidate_bp = Blueprint('candidate', __name__)


@candidate_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response


@candidate_bp.route('/save_candidate', methods=['POST'])
@cross_origin(origins='http://localhost:3000')
def save_candidate():
    data = request.json
    if data:
        # Generate a unique candidate ID
        candidate_id = str(uuid.uuid4())
        # Add the candidate ID to the data
        data['candidate_id'] = candidate_id
        # Save the data to the MongoDB collection
        try:
            result = candidate_collection.insert_one(data)
            print(f"Inserted document ID: {result.inserted_id}")
            return jsonify({"message": "Candidate saved successfully", "candidate_id": candidate_id}), 201
        except Exception as e:
            print(f"Error inserting document: {e}")
            return jsonify({"message": "Failed to save candidate"}), 500
    else:
        return jsonify({"message": "No data provided"}), 400



@candidate_bp.route('/get_candidate', methods=['GET'])
def get_candidate():
    try:
        collection = candidate_collection
        documents = collection.find()# Retrieve all documents
        schedules = []
        for doc in documents:
            doc['_id'] = str(doc['_id'])  # Convert ObjectId to string
            schedules.append(doc)

        return jsonify(schedules), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500