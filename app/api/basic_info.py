from flask import Flask, request, jsonify,Blueprint
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus
from app.db.mongo_client import basic_info_collection



basicInfo_bp = Blueprint('basic_info', __name__, url_prefix='/api/basic_info')
 
@basicInfo_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response



@basicInfo_bp.route('/basic_info', methods=['POST'])
def save_basic_info():
    data = request.json
    if not data:
        return jsonify({"error": "No data provided"}), 400

    try:
        basic_info_collection.insert_one(data)
        return jsonify({"message": "Data saved successfully"}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@basicInfo_bp.route('/get_basic_info', methods=['GET'])
def get_basic_info():
    try:
        data = list(basic_info_collection.find({}, {'_id': 0}))
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500

