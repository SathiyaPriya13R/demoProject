from flask import Flask, request, jsonify,Blueprint
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus
from app.db.mongo_client import marketing_collection
from bson import ObjectId

marketing_bp = Blueprint('marketing', __name__, url_prefix='/api/marketing')
 
@marketing_bp.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return response


@marketing_bp.route('/marketing_info', methods=['POST'])
def save_marketing_info():
    try:
        data = request.get_json()
        marketing_collection.insert_one(data)
        return jsonify({'message': 'Marketing information saved successfully'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@marketing_bp.route('/get_marketing', methods=['GET'])
def get_marketing_info():
    try:
        marketing_info = list(marketing_collection.find())
        # Convert ObjectId to string for JSON serialization
        for doc in marketing_info:
            doc['_id'] = str(doc['_id'])
        print("Fetched marketing information from MongoDB:", marketing_info)
        return jsonify(marketing_info), 200
    except Exception as e:
        print("Error fetching marketing information:", str(e))
        return jsonify({'error': str(e)}), 500



