from flask import Blueprint, request, jsonify
from app.db.mongo_client import get_schedule_collection
from app.db.mongo_client import get_questions_collection

schedule_bp = Blueprint('schedule', __name__)

@schedule_bp.route('/save', methods=['POST'])
def save_schedule():
    try:
        schedule_data = request.json
        collection = get_schedule_collection()
        result = collection.insert_one(schedule_data)
        inserted_id = str(result.inserted_id)
        return jsonify({"message": "Schedule data saved successfully", "inserted_id": inserted_id}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    
@schedule_bp.route('/get_schedule', methods=['GET'])
def get_schedules():
    try:
        collection = get_schedule_collection()
        documents = collection.find()
        schedules = []
        for doc in documents:
            doc['_id'] = str(doc['_id'])
            schedules.append(doc)
        return jsonify(schedules), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500

 
@schedule_bp.route('/get_questions/<primary_skills>', methods=['GET'])
def get_questions(primary_skills):
    try:
        print(primary_skills)
        collection = get_questions_collection()
        additional_data_collection = collection[primary_skills]
        documents = additional_data_collection.find()  # Retrieve all documents
        schedules = []
        for doc in documents:
            doc['_id'] = str(doc['_id'])  # Convert ObjectId to string
            schedules.append(doc)

        return jsonify(schedules), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
        