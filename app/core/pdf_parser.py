from pdfminer.high_level import extract_text
from app.core.extractors import extract_name, extract_contact_number, extract_email, extract_skills, extract_education, extract_cities, extract_hobbies, extract_states, extract_linkedin_info

def parse_resume(file_path):
    text = extract_text(file_path)

    skills_list = ['Python', 'Data Analysis', 'Machine Learning', 'Communication', 'Project Management', 'Deep Learning', 'SQL', 'Tableau']
    cities_list = ['Hyderabad', 'Mumbai', 'Bangalore', 'Chennai', 'Kolkata', 'Delhi']
    hobbies_list = ['drawing', 'painting', 'sketching', 'photography']
    indian_states_list = ['Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand', 'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura', 'Uttar Pradesh', 'Uttarakhand', 'West Bengal', 'Andaman and Nicobar Islands', 'Chandigarh', 'Dadra and Nagar Haveli and Daman and Diu', 'Lakshadweep', 'Delhi', 'Puducherry']

    parsed_data = {
        'name': extract_name(text),
        'contact_number': extract_contact_number(text),
        'email': extract_email(text),
        'skills': extract_skills(text, skills_list),
        'education': extract_education(text),
        'cities': extract_cities(text, cities_list),
        'hobbies': extract_hobbies(text, hobbies_list),
        'indian_states': extract_states(text, indian_states_list),
        'linkedin_ids': extract_linkedin_info(text)[0],
        'linkedin_urls': extract_linkedin_info(text)[1]
    }

    return parsed_data
