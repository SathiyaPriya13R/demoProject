import re
import spacy
from spacy.matcher import Matcher

def extract_contact_number(text):
    pattern = r"\b(?:\+?\d{1,3}[-.\s]?)?\(?\d{3}\)?[-.\s]?\d{3}[-.\s]?\d{4}\b"
    match = re.search(pattern, text)
    return match.group() if match else None

def extract_email(text):
    pattern = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b"
    match = re.search(pattern, text)
    return match.group() if match else None

def extract_skills(text, skills_list):
    skills = []
    for skill in skills_list:
        pattern = r"\b{}\b".format(re.escape(skill))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            skills.append(skill)
    return skills

def extract_education(text):
    pattern = r"(?i)(?:Bsc|\bB\.\w+|\bM\.\w+|\bPh\.D\.\w+|\bBachelor(?:'s)?|\bMaster(?:'s)?|\bPh\.D)\s(?:\w+\s)*\w+"
    return re.findall(pattern, text)

def extract_name(text):
    nlp = spacy.load('en_core_web_sm')
    matcher = Matcher(nlp.vocab)
    patterns = [
        [{'POS': 'PROPN'}, {'POS': 'PROPN'}],
        [{'POS': 'PROPN'}, {'POS': 'PROPN'}, {'POS': 'PROPN'}],
        [{'POS': 'PROPN'}, {'POS': 'PROPN'}, {'POS': 'PROPN'}, {'POS': 'PROPN'}]
    ]
    for pattern in patterns:
        matcher.add('NAME', [pattern])
    doc = nlp(text)
    matches = matcher(doc)
    for match_id, start, end in matches:
        span = doc[start:end]
        return span.text
    return None

def extract_cities(text, cities_list):
    cities = []
    for city in cities_list:
        pattern = r"\b{}\b".format(re.escape(city))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            cities.append(city)
    return cities

def extract_hobbies(text, hobbies_list):
    hobbies = []
    for hobby in hobbies_list:
        pattern = r"\b{}\b".format(re.escape(hobby))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            hobbies.append(hobby)
    return hobbies

def extract_states(text, indian_states_list):
    states = []
    for state in indian_states_list:
        pattern = r"\b{}\b".format(re.escape(state))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            states.append(state)
    return states

def extract_linkedin_info(text):
    id_pattern = r'(linkedin\.com\/in\/[a-zA-Z0-9_-]+)'
    url_pattern = r'(https?:\/\/(?:www\.)?linkedin\.com\/in\/[a-zA-Z0-9_-]+)'
    ids = re.findall(id_pattern, text)
    urls = re.findall(url_pattern, text)
    return ids, urls
