from pymongo import MongoClient
from app.config import Config
import urllib.parse
 
def get_mongo_client():
    username = urllib.parse.quote_plus(Config.MONGO_USERNAME)
    password = urllib.parse.quote_plus(Config.MONGO_PASSWORD)
    connection_string = f"mongodb+srv://{username}:{password}@{Config.MONGO_URI}/?retryWrites=true&w=majority&appName=AtlasCluster"
    return MongoClient(connection_string)
 
def get_users_collection():
    client = get_mongo_client()
    db = client['ViTech']  # Replace with your database name
    return db['user']
 
def get_schedule_collection():
    client = get_mongo_client()
    db = client["ViTech"]
    return db["int_schedule"]
 
def get_email_collection():
    client = get_mongo_client()
    db = client["ViTech"]
    return db["demo_status"]


def get_candidate_collection():
    client = get_mongo_client()
    db = client["ViTech"]
    return db["Candidate"]

def get_questions_collection():
    client = get_mongo_client()
    db = client["InterviewQuestions"]
    return client["InterviewQuestions"]

def get_basic_info_collection():
    client = get_mongo_client()
    db = client["AgencyProfile"]
    return db["Basis_info"]

def get_contact_collection():
    client = get_mongo_client()
    db = client["AgencyProfile"]
    return db["contact_info"]

def get_certificate_collection():
    client = get_mongo_client()
    db = client["AgencyProfile"]
    return db["certificate"]

def get_additional_collection():
    client = get_mongo_client()
    db = client["AgencyProfile"]
    return db["additonaldoc"]

def get_marketing_collection():
    client = get_mongo_client()
    db = client["AgencyProfile"]
    return db["marketing"]

# For convenience, you can also instantiate the collections here
users_collection = get_users_collection()
schedule_collection = get_schedule_collection()
email_collection = get_email_collection()
candidate_collection = get_candidate_collection()
basic_info_collection = get_basic_info_collection()
contact_collection = get_contact_collection()
certificate_collection = get_certificate_collection()
additional_collection = get_additional_collection()
marketing_collection = get_marketing_collection()
